---- Minecraft Crash Report ----
// But it works on my machine.

Time: 2/17/18 10:28 PM
Description: Exception ticking world

java.util.ConcurrentModificationException
	at java.util.HashMap$HashIterator.remove(HashMap.java:1456)
	at on.d(SourceFile:198)
	at oo.d(SourceFile:206)
	at net.minecraft.server.MinecraftServer.D(SourceFile:624)
	at nz.D(SourceFile:349)
	at net.minecraft.server.MinecraftServer.C(SourceFile:560)
	at net.minecraft.server.MinecraftServer.run(SourceFile:464)
	at java.lang.Thread.run(Thread.java:748)


A detailed walkthrough of the error, its code path and all known details is as follows:
---------------------------------------------------------------------------------------

-- Head --
Thread: Server thread
Stacktrace:
	at java.util.HashMap$HashIterator.remove(HashMap.java:1456)
	at on.d(SourceFile:198)
	at oo.d(SourceFile:206)

-- Affected level --
Details:
	Level name: world
	All players: 1 total; [oq['MikeZhan'/644, l='world', x=-1043.18, y=67.00, z=-673.20]]
	Chunk stats: ServerChunkCache: 803 Drop: 145
	Level seed: -2153536881822275630
	Level generator: ID 00 - default, ver 1. Features enabled: true
	Level generator options: 
	Level spawn location: World: (52,63,-168), Chunk: (at 4,3,8 in 3,-11; contains blocks 48,0,-176 to 63,255,-161), Region: (0,-1; contains chunks 0,-32 to 31,-1, blocks 0,0,-512 to 511,255,-1)
	Level time: 12433848 game time, 1281095 day time
	Level dimension: 0
	Level storage version: 0x04ABD - Anvil
	Level weather: Rain time: 118933 (now: false), thunder time: 40926 (now: false)
	Level game mode: Game mode: survival (ID 0). Hardcore: false. Cheats: false
Stacktrace:
	at net.minecraft.server.MinecraftServer.D(SourceFile:624)
	at nz.D(SourceFile:349)
	at net.minecraft.server.MinecraftServer.C(SourceFile:560)
	at net.minecraft.server.MinecraftServer.run(SourceFile:464)
	at java.lang.Thread.run(Thread.java:748)

-- System Details --
Details:
	Minecraft Version: 1.12.2
	Operating System: Linux (arm) version 3.10.102
	Java Version: 1.8.0_151, Oracle Corporation
	Java VM Version: OpenJDK Client VM (mixed mode), Oracle Corporation
	Memory: 656219720 bytes (625 MB) / 859045888 bytes (819 MB) up to 859045888 bytes (819 MB)
	JVM Flags: 5 total; -Xmx832M -Xms832M -XX:+UseConcMarkSweepGC -XX:+CMSIncrementalPacing -XX:+AggressiveOpts
	IntCache: cache: 0, tcache: 0, allocated: 13, tallocated: 95
	Profiler Position: N/A (disabled)
	Player Count: 1 / 20; [oq['MikeZhan'/644, l='world', x=-1043.18, y=67.00, z=-673.20]]
	Is Modded: Unknown (can't tell)
	Type: Dedicated Server (map_server.txt)